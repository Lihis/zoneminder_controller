#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <termios.h>
#include <errno.h>

#define SERIAL_PORT "/dev/ttyUSB0"

#define CPU_TEMP_READING "/sys/class/hwmon/hwmon2/device/temp2_input"

char *zmpkg_path = "/usr/bin/zmpkg.pl";

int run_state()
{
	int pipefd[2];
	pid_t pid;
	char buf[7];
	int status;

	if (pipe(pipefd) == -1) {
		printf("INFO: Pipe error. ERRNO: %s\n", strerror(errno));
		return -1;
	}

	pid = fork();
	if (pid == -1) {
		printf("INFO: Fork error.\n");
	} else if (pid == 0) {
		dup2(pipefd[1], STDOUT_FILENO);
		close(pipefd[0]);
		close(pipefd[1]);

		execl(zmpkg_path, "zmpkg.pl", "status", (char *)0);
		_exit(1);
	} else {
		close(pipefd[1]);
		read(pipefd[0], buf, sizeof(buf));
		close(pipefd[0]);
		if (wait(&status) < 0) {
			if (WIFEXITED(status)) {
				printf("Exited with error: %d\n", WEXITSTATUS(status));
				return -1;
			}
			if (WIFSIGNALED(status)) {
				printf("Killed via signal %d\n", WTERMSIG(status));
				return -1;
			}
		}

		if (strcmp(buf, "running") == 0) {
			printf("Running\n");
			return 1;
		} else if (strcmp(buf, "stopped") == 0) {
			printf("Stopped.\n");
			return 0;
		} else {
			printf("ERROR: State \"%s\"\n", buf);
			return -1;
		}
	}

	return -1;
}

void change_state(char state)
{
	pid_t pid;
	int status;
	char *command;

	switch(state) {
	case '0':
		command = "stop";
		break;
	case '1':
		command = "start";
		break;
	default:
		command = "status";
		break;
	}

	pid = fork();
	if (pid == -1) {
		printf("INFO: Fork error\n");
	} else if (pid == 0) {
		execl(zmpkg_path, "zmpkg.pl", command, (char *)0);
		_exit(1);
	} else {
		if (wait(&status) < 0) {
			if (WIFEXITED(status)) {
				printf("Exited with error: %d\n", WEXITSTATUS(status));
//				return -1;
			}
			if (WIFSIGNALED(status)) {
				printf("Killed via signal %d\n", WTERMSIG(status));
//				return -1;
			}
		}
	}
}

int main(int argc, char **argv)
{
	int serial_port_fd;

	serial_port_fd = open(SERIAL_PORT, O_RDWR | O_NOCTTY);
	if (serial_port_fd == -1) {
		printf("Unable to open port \"%s\"\n", SERIAL_PORT);
	} else {
		printf("INFO: Serial port connection opened.\n");
	}

	int errno;
	errno = 0;

	/* serial_port_fd SETTINGS */
	struct termios tty;
	memset (&tty, 0, sizeof tty);

	/* Error Handling for reading port current parameters */
	if (tcgetattr(serial_port_fd, &tty) != 0) {
		printf("Unable to read serial port current parameters.\n");
		return 1;
	}

	// NO PARITY
	tty.c_cflag	&=  ~PARENB;		// No parity
	tty.c_cflag	&=  ~CSTOPB;		// 1 stop bit
	tty.c_cflag	&=  ~CSIZE;		// Mask the character size bits
	tty.c_cflag	|=  CS8;		// 8-bit data bits
	tty.c_iflag	&= ~BRKINT;		// Disable BRKINT
	tty.c_iflag	&=  ~ICRNL;		// Do not translate carriage return to new line
	tty.c_iflag	&=  ~IMAXBEL;		// Ring bell when queue full (not in linux)
	tty.c_oflag	&=  ~OPOST;		// Disable implementation-defined output processing
	tty.c_lflag	&=  ~ISIG;		// Do not create INTR, QUIT, SUSP or DSUSP signals
	tty.c_lflag	&=  ~ICANON;		// Disable canonical mode
	tty.c_lflag	&=  ~IEXTEN;		// Disable imlementation-defined input processing
	tty.c_lflag	&=  ~ECHO;		// Disable echo input characters
	tty.c_lflag	&=  ~ECHOE;
	tty.c_lflag	&=  ~ECHOK;		// KILL character do not erase current line
	tty.c_lflag	&=  ~ECHOCTL;		// (not in posix)
	tty.c_lflag	&=  ~ECHOKE;		// (not in posix)

	tty.c_cflag	&=  ~CRTSCTS;		// no flow control
	tty.c_cc[VMIN]	=   1;			// read do not block
	tty.c_cc[VTIME]	=   0;			// 0.5 second read timeout
	tty.c_cflag	|=  CREAD | CLOCAL;	// enable the receiver and set local mode

	/* Set Baud Rate */
	cfsetospeed (&tty, (speed_t)B9600);
	cfsetispeed (&tty, (speed_t)B9600);

	/* Flush Port and apply port parameters */
	tcflush( serial_port_fd, TCIFLUSH );
	if ( tcsetattr ( serial_port_fd, TCSANOW, &tty ) != 0) {
		printf("Unable to set serial port parameters.\n");
		return 1;
	}

	// Arduino resets when we connect it so wait couple 
	// seconds before sending any data to it.
	sleep(2);

	// Loop and wait commands
	while (1) {
		// ZoneMinder running state
//		int status;

		// Get state
//		status = run_state();

		// Send it to arduino
//		write(serial_port_fd, &status, sizeof(int));

		//sleep(1);

		char buf[24] = {'\0'};
		char cur_state;

		read(serial_port_fd, buf, sizeof(buf) - 1);
		printf("%s\n", buf);

		if(strcmp(buf, "run_state") == 0) {
			cur_state = run_state();
			write(serial_port_fd, &cur_state, sizeof(cur_state));
		}

		if (strncmp(buf, "change_run_state", 16) == 0) {
			switch(buf[17]) {
			case '0':
				change_state(buf[17]);
				cur_state = run_state();
				write(serial_port_fd, &cur_state, sizeof(cur_state));
				break;
			case '1':
				change_state(buf[17]);
				cur_state = run_state();
				write(serial_port_fd, &cur_state, sizeof(cur_state));
				break;
			default:
				printf("huh? '%c'\n", buf[17]);
				break;
			}
		}

		if (strncmp(buf, "lock F", 6) == 0) {
			printf("write lock\n");
			char *lock = "lock 0";
			write(serial_port_fd, lock, sizeof(lock));
		}

		//if (strcmp(buf, "run_state") == 0) {
		//	printf("Sendind state..\n");
		//	status = run_state();
		//	write(serial_port_fd, "1\r\n", 5);
		//}

		// Sleep berfore looping again
//		sleep(1);
	}

	return 0;
}
