#include <LiquidCrystal.h>
// Initialize LCD (rs, e, d4, d5, d6 ,d7)
LiquidCrystal lcd(8, 9, 5, 6, 7, 3);

// Timers for LCD
unsigned long lcd_off_timer;
// Time after to turn off the LCD (milliseconds)
unsigned long lcd_off_time = 15000;
unsigned long lcd_update_timer;
// LCD update interval
unsigned long lcd_update_interval = 5000;
// Timer for read timeout
unsigned long read_timeout = 2000;
// Timers for buttons presses
unsigned long select_timer;
unsigned long left_timer;
unsigned long right_timer;

// Screen
int screen = 1;

// Title line offset
const int title = 2;
const int offset = 2;

char *command;
float value;

// ZoneMinder run state
int zm_run_state;

// Backlight pin
const int backlight_pin = A4;

// Select button pin
const int select_button_pin = A2;
// Left button pin
const int left_button_pin = A3;
// Right button pin
const int right_button_pin = A1;

// Declare some functions
void draw_display();
int get_lock();

void setup() {
	Serial.begin(9600);

	// Set the number of columns and rows
	lcd.begin(16, 2);

	// Print splash
	lcd.setCursor(2, 0);
	lcd.print("ZM Controller");
	lcd.setCursor(3, 1);
	lcd.print("(c)  Lihis");
	lcd.display();

	// Set backlight ON
	pinMode(backlight_pin, OUTPUT);
	digitalWrite(backlight_pin, HIGH);

	// Show some splash first
	delay(1000);

	// Set run state
	zm_run_state = -1;

	// Initalize buttons
	pinMode(select_button_pin, INPUT_PULLUP);
	pinMode(left_button_pin, INPUT_PULLUP);
	pinMode(right_button_pin, INPUT_PULLUP);

	// Save the time
	lcd_off_timer = millis();
	select_timer = millis();

	// Call draw display
	draw_display();
}

void lcd_off()
{
	lcd_off_timer = millis();
	digitalWrite(backlight_pin, LOW);
	lcd.clear();

	switch(screen) {
	case 2:
		screen = 1;
		break;
	default:
		break;
	}
}

void draw_status()
{
        // Clear the display
        lcd.clear();
        lcd.setCursor(title,0);

        // Title
        lcd.print("1 TILA");

        // State
	char *state;

	// 0 for stopped
	// 1 for running
	// 10 for arduino waiting response from client
	// -1 for waiting pc connection (default)
	switch(zm_run_state) {
	case 0:
		state = (char *)"Stopped";
		break;
	case 1:
		state = (char *)"Running";
		break;
	case 10:
		state = (char *)"Odotetaan..";
		break;
	default:
		state = (char *)"Waiting PC..";
		break;
	}

	lcd.setCursor(offset,1);
	lcd.print(state);
}

void draw_change_status(int status)
{
	if ((zm_run_state != 0) && (zm_run_state != 1)) {
		screen = 1;
		return;
	}

	lcd.clear();
        lcd.setCursor(title,0);
        lcd.print("VAIHDA TILA");
        lcd.setCursor(offset - 1,1);

	int lock_state;

	lock_state = get_lock();
	if (lock_state != 0) {
		lcd.print("Laite lukittu.");
		delay(5000);
		screen = 1;
		return;
	}

        switch(status) {
        case 0:
                lcd.print("[START]  STOP");
                break;
        case 1:
                lcd.print(" START  [STOP]");
                break;
        default:
		screen = 1;
                break;
        }
}

void get_run_state()
{
	unsigned long prev_millis = millis();
	int response = 0;

	Serial.write("run_state");

	while (millis() - prev_millis < read_timeout) {
		if (Serial.available() > 0) {
			zm_run_state = Serial.read();
			response = 1;
			break;
		}
	}

	if (response == 0)
		zm_run_state = -1;
	
	delay(400);

	Serial.print("run_state OK ");
	Serial.print(zm_run_state, DEC);

	delay(500);
}

void change_run_state(int state)
{
	unsigned long prev_millis;
//	int response = 0;

	// if state 0 (stopped) send 1 to start
	// if state 1 (running) send 0 to stop
	switch(state) {
		case 0:
			state = 1;
			break;
		case 1:
			state = 0;
			break;
		default:
			break;
	}

	Serial.print("change_run_state ");
	Serial.print(state, DEC);

	prev_millis = millis();

	zm_run_state = 10;
	draw_status();

	while (millis() - prev_millis < 20000) {
		if (Serial.available() > 0) {
			zm_run_state = Serial.read();
			//response = 1;
	//		Serial.print("change_run_state OK");
			break;
		}
	}

	lcd_off_timer = millis();

//	if (response == 0)
//		Serial.print("change_run_state NG");

	delay(500);

	// Set screen back to TILA
	screen = 1;
}

int get_lock()
{
	Serial.write("lock F");

	delay(500);

	int availableBytes = Serial.available();
	char response[24];
	for (int i = 0; i < availableBytes; i++) {
		response[i] = Serial.read();
		response[i+1] = '\0';
	}

	if (strncmp(response, "lock 0", 6) == 0) {
		return 0;
	} else {
		return 1;
	}

	return 1;
}

void draw_lock()
{
	lcd.clear();
	lcd.setCursor(title,0);
	lcd.print("2 LUKITUS");
	lcd.setCursor(offset,1);

	int lock_state = get_lock();

	switch(lock_state) {
	case 0:
		lcd.print("Laite avoin");
		break;
	default:
		lcd.print("Laite lukittu");
		break;
	}
}

void draw_display()
{
	digitalWrite(backlight_pin, HIGH);

	// 1  TILA
	// 2  VAIHDA TILA
	switch(screen) {
	case 1:
		get_run_state();
		draw_status();
		break;
	case 2:
		draw_change_status(zm_run_state);
		break;
	case 3:
		draw_lock();
		break;
	default:
		break;
	}
}

void select_pressed()
{
	switch(screen) {
	case 1:
		screen = 2;
		delay(500);
		break;
	case 2:
		change_run_state(zm_run_state);
		delay(500);
		break;
	default:
		break;
	}
	draw_display();
}

void left_pressed()
{
	switch(screen) {
	case 2:
		screen = 1;
		break;
	case 3:
		screen = 1;
		break;
	default:
		break;
	}
	draw_display();
}

void right_pressed()
{
	switch(screen) {
	case 1:
		screen = 3;
	case 2:
		switch(zm_run_state) {
		case 0:
			zm_run_state = 1;
			break;
		case 1:
			zm_run_state = 0;
			break;
		default:
			break;
		}
		delay(250);
		break;
	default:
		break;
	}
	draw_display();
}

void loop() {
	// Read all pins and buttons state
	int backlight_state = digitalRead(backlight_pin);
	int select_button = digitalRead(select_button_pin);
	int left_button = digitalRead(left_button_pin);
	int right_button = digitalRead(right_button_pin);

	// Draw display if the screen is on
//	if ((backlight_state == HIGH) && (millis() - lcd_update_timer > lcd_update_interval)) {
//		draw_display();
//		lcd_update_timer = millis();
//	}

	// Turn off the LCD and clear it
	if ((backlight_state == HIGH) && (millis() - lcd_off_timer > lcd_off_time)) {
		lcd_off();
	}

	// Turn LCD on if it's off and button is pushed
	if ((backlight_state == LOW) && ((select_button == LOW) || (left_button == LOW) || (right_button == LOW))) {
		draw_display();
		lcd_off_timer = millis();
	}

	if ((select_button == LOW) && (millis() - select_timer >= 5) && (backlight_state == HIGH)) {
		lcd_off_timer = millis();
		select_timer = millis();
		select_pressed();
	}

	if ((left_button == LOW) && (millis() - left_timer >= 5) && (backlight_state == HIGH)) {
		lcd_off_timer = millis();
		left_timer = millis();
		left_pressed();
	}

	if ((right_button == LOW) && (millis() - right_timer >= 5) && (backlight_state == HIGH)) {
		lcd_off_timer = millis();
		right_timer = millis();
		right_pressed();
	}
 
}
